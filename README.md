# PyFoReL

The **Py**thonic **Fo**rmal **Re**quirements **L**anguage (PyFoReL) tool is a Domain-Specific Language inspired by the programming language Python to simplify the elicitation of TL-based requirements for engineers and non-experts.

## Getting Started

To get started, the following dependencies must be installed in order to build the tool:

1. CMake v3.18+
2. C++ Compiler (e.g., GNU GCC)
3. UUID Library Developer's Package (`libuuid-devel` on RHEL and `uuid-dev` on Ubuntu)
4. Java JDK v1.8.0+
5. Git

With the dependencies installed, you can now build the PyFoReL tool with the following command (assuming you are in the project's root directory):

```bash
cmake -B build
cmake --build build
```

It will take a little bit to generate the parser from the grammar and compile the tool, so you can pass the flag `--parallel=J` where `J` is the number of jobs to run in parallel that is less than or equal to the number of threads on your CPU.

## Running PyFoReL

PyFoReL can be ran from two types of input sources:

1. File
2. Stdin (Standard Input)

To pass a file to PyFoReL, call the command with the path to the file as the first argument. Optionally, you can pass the `-o` flag followed by a filename to write the resulting translation to. For example:

```bash
pyforel req.pf -o formula.txt
```

To use PyFoReL from standard input, call the command `pyforel` with no arguments. When you are done writing your PyFoReL program, you can translate your program by terminating the input with the keystroke `Ctrl-D`.

## Language Support

Currently, a Visual Studio Code extension to support writing PyFoReL programs is under development (i.e., not available on the marketplace). However, useful features such as syntax highlight, autocompletion, and snippet insertion for PyFoReL programs are supported. If you are interested, you can find it [here](https://github.com/andersonjwan/vscode-pyforel).
