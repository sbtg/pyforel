#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>

#include "antlr4-runtime.h"

/*** STPL DSL components ***/
#include "StplDslInterface.h"
#include "StplDslErrorListenerInterface.h"
#include "StplDslLexer.h"
#include "StplDslParserBaseListener.h"

/*** STPL components ***/
#include "StplInterface.h"
#include "StplErrorListenerInterface.h"
#include "StplLexer.h"
#include "StplParserBaseListener.h"

using namespace antlr4;

int
main(int argc, char **argv) {
    int oflag = 0;
    char *ovalue = NULL;
    int c;

    opterr = 0;

    // handle options
    while((c = getopt(argc, argv, "o:")) != -1) {
        switch(c) {
        case 'o':
            oflag = 1;
            ovalue = optarg;
            break;
        case '?':
            if(optopt == 'o') {
                std::cerr << "Option " << "-"
                          << (char) optopt
                          << " requires and argument." << std::endl;
            } else if(isprint(optopt)) {
                std::cerr << "Unknown option `-"
                          << (char) optopt << "'." << std::endl;
            } else {
                std::cerr << "Unknown option character `\\x";
                std::cerr << std::hex << optopt;
                std::cerr << "'." << std::endl;
            }

            return 1;
        default:
            abort();
        }
    }

    // ANTLRInputStream Object
    ANTLRInputStream input;

    // check if file passed
    if(argc > 1) {
        int count = 0, index;
        for(index = optind; index < argc; ++index)
            ++count;

        if(count > 1) {
            std::cerr << "Unexpected additional input files found. "
                      << "One input file per translation."
                      << std::endl;
        } else if(count == 0) {
            std::cerr << "Expected an input file to translate." << std::endl;
        } else {
            char *file;
            file = argv[optind];

            std::ifstream stream;
            stream.open(file);

            input = ANTLRInputStream(stream);
        }
    } else {
        input = ANTLRInputStream(std::cin);
    }

    // DSL -> STPL Translation Step
    StplDslLexer dsl_lexer(&input);
    CommonTokenStream dsl_tokens(&dsl_lexer);
    StplDslInterface dsl_parser(&dsl_tokens);

    dsl_parser.removeErrorListeners();
    StplDslErrorListenerInterface *dsl_error = new StplDslErrorListenerInterface();
    dsl_parser.addErrorListener(dynamic_cast<antlr4::ANTLRErrorListener*>(dsl_error));
    dsl_parser.setErrorInterface(dsl_error);

    tree::ParseTree *dsl_tree = dsl_parser.stpl_program();
    StplDslParserBaseListener dsl_listener;
    tree::ParseTreeWalker::DEFAULT.walk(&dsl_listener, dsl_tree);

    // check if any errors were found during parsing
    if(dsl_error->getErrorCount() > 0)
        return 1;

    std::string result;
    result = dsl_parser.getTranslation();

    std::stringstream tr_stream;
    tr_stream.str(result);

    ANTLRInputStream translation = ANTLRInputStream(tr_stream);

    // STPL Type/Error Checking Step
    StplLexer stpl_lexer(&translation);
    CommonTokenStream stpl_tokens(&stpl_lexer);
    StplInterface stpl_parser(&stpl_tokens);

    stpl_parser.removeErrorListeners();
    StplErrorListenerInterface *stpl_error = new StplErrorListenerInterface();
    stpl_parser.addErrorListener(dynamic_cast<antlr4::ANTLRErrorListener *>(stpl_error));
    stpl_parser.setErrorInterface(stpl_error);

    tree::ParseTree *stpl_tree = stpl_parser.stplSpecification();
    StplParserBaseListener stpl_listener;
    tree::ParseTreeWalker::DEFAULT.walk(&stpl_listener, stpl_tree);

    // check if any errors were found during parsing
    if(stpl_error->getErrorCount() > 0)
        return 2;

    // print / write translation
    if(oflag)
        dsl_parser.writeTranslation(ovalue);
    else
        dsl_parser.printTranslation();

    return 0;
}
