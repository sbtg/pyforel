parser grammar StplDslParser;
import StplDslParserActions;

options {
    language = Cpp ;
    tokenVocab = StplDslLexer ;
}

tokens {INDENT, DEDENT}

stpl_program
    : (NEWLINE | stmt             {appendStatement($stmt.ret);}
        )* EOF
;

stmt
    returns [
        std::string ret
    ]

    : simple_stmt                 {$ret = $simple_stmt.ret;}
    | compound_stmt               {$ret = $compound_stmt.ret;}
;

simple_stmt
    returns [
        std::string ret
    ]

    : (stmt1=small_stmt           {$ret = conjunctSimpleStmts($ret, $stmt1.ret);}
    ) (SEMICOLON stmtN=small_stmt       {$ret = conjunctSimpleStmts($ret, $stmtN.ret);}
    )* (SEMICOLON)? NEWLINE
;

small_stmt
    returns [
        std::string ret
    ]

    : flow_stmt                   {$ret = $flow_stmt.ret;}
    | func_stmt                   {$ret = $func_stmt.ret;}
    | import_stmt
;

flow_stmt
    returns [
        std::string ret
    ]

    : continue_stmt               {$ret = $continue_stmt.ret;} ;

continue_stmt
    returns [
        std::string ret
    ]

    : CONTINUE                  {$ret = "continue";} ;

func_stmt
    returns [
        std::string ret
    ]

    locals [
        std::vector<std::string> args
    ]

    : func=NAME LPAREN (id1=NAME     {$args.push_back($id1.text);}
        )? (COMMA idN=NAME          {$args.push_back($idN.text);}
        )* RPAREN                    {$ret = getFuncTranslation($func, $args);};

import_stmt
    : import_name ;

import_name
    : IMPORT dotted_as_names ;

dotted_as_names
    : dotted_as_name (COMMA dotted_as_name)* ;

dotted_as_name
    : dotted_name (AS NAME)? ;

dotted_name
    : NAME (DOT NAME)* ;

verb_stmt
    returns [
        std::string ret
    ]

    : VERB COLON NEWLINE INDENT
        STRING NEWLINE DEDENT {$ret = $STRING.text;}
;

compound_stmt
    returns [
        std::string ret
    ]

    : if_stmt {$ret = $if_stmt.ret;}
    | func_def
    | verb_stmt {$ret = reduceString($verb_stmt.ret);}
    | freezetime_stmt {$ret = $freezetime_stmt.ret;}

    | future_stmt {$ret = $future_stmt.ret;}
;

if_stmt
    returns [
        std::string ret
    ]

    locals [
        std::string else_body,
        std::string else_cond
    ]

    : IF ifcond=condition COLON ifbody=body
    (ELIF elcond=condition COLON elbody=body
        {$ret = createElifStmt($ret, $ifcond.ret, $elcond.ret, $elbody.ret);}
    )*
    (ELSE COLON elsbody=body                 {$else_body = $elsbody.ret;})?
    {$ret = createIfStmt($ret, $ifcond.ret, $ifbody.ret, $else_body);}
;

freezetime_stmt
    returns [
        std::string ret
    ]

    : quant=(EXISTS | FORALL) (id1=NAME {$ret = translateNameList($ret, $id1);}
    ) (COMMA idN=NAME                         {$ret = translateNameList($ret, $idN);}
    )* (AT time=NAME)? COLON body           {$ret = translateFreezeTime($ret, $quant.text, $time.text, $body.ret);}
;

func_def
    locals [
        std::vector<std::string> params
    ]

    : FUNC NAME LPAREN (parameters {$params = $parameters.ret;}
    )? RPAREN COLON body
        {addFuncDefinition($NAME, $params, $body.ret);}
;

future_stmt
    returns [
        std::string ret
    ]

    : op=TEMPORAL_OP
      (FROM lower=NUMBER TO upper=NUMBER
            {$ret = translateBounds($lower.text, $upper.text);}
      )? COLON body
        {$ret = translateFutureTemporal($ret, $op.text, $body.ret);}
;

parameters
    returns [
        std::vector<std::string> ret
    ]

    : typed_args_list {$ret = $typed_args_list.ret;};

typed_args_list
    returns [
        std::vector<std::string> ret
    ]

    : (TYPE param1=tfpdef        {$ret.push_back($param1.text);}
        ) (COMMA TYPE paramN=tfpdef {$ret.push_back($paramN.text);}
        )*
;

tfpdef
    : NAME ;

body
    returns [
        std::string ret
    ]

    locals [
        std::string stmts
    ]

    : simple_stmt {$ret = $simple_stmt.ret;}
    | NEWLINE INDENT (stmt {$stmts = $stmts.empty() ? parenthesize($stmt.ret) : $stmts + " && " + parenthesize($stmt.ret);})+ DEDENT {$ret = parenthesize($stmts);}
;

condition
    returns [
        std::string ret
    ]

    : or_cond {$ret = $or_cond.ret;}
;

or_cond
    returns [
        std::string ret
    ]

    locals [
        std::string sec
    ]

    : op1=and_cond
    (OR opn=and_cond {$sec = $sec.empty() ? " or " + $opn.ret : $sec + " or " + $opn.ret;})*
    {
        $ret = $op1.ret;

        if(!($sec.empty())) {
            $ret = $ret + $sec;
        }
    }
;

and_cond
    returns [
        std::string ret
    ]

    locals [
        std::string sec
    ]

    : op1=not_cond
    (AND opn=not_cond {$sec = $sec.empty() ? " and " + $opn.ret : $sec + " and " + $opn.ret;})*
    {
        $ret = $op1.ret;

        if(!($sec.empty())) {
            $ret = $ret + $sec;
        }
    };

not_cond
    returns [
        std::string ret
    ]

    : NOT not_cond {$ret = "not " + $not_cond.ret;}
    | comparison {$ret = $comparison.ret;}
;

comparison
    returns [
        std::string ret
    ]

    : func_stmt {$ret = $func_stmt.ret;}
    | BOOLEAN   {$ret = $BOOLEAN.text;}
;

comp
    : OR | AND ;
