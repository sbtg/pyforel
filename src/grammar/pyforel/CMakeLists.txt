antlr_target(pyforelLexer StplDslLexer.g4 LEXER)
antlr_target(pyforelParser StplDslParser.g4 PARSER
  DEPENDS_ANTLR pyforelLexer
  COMPILE_FLAGS -lib ${ANTLR_pyforelLexer_OUTPUT_DIR})
