lexer grammar StplDslLexerActions;

@lexer::postinclude {
    #include <string>
    #include <iostream>

    #include "StplDslParser.h"
}

@lexer::members {
    std::vector<std::unique_ptr<antlr4::Token>> m_tokens;
    std::stack<int>                             m_indents;

    int m_opened = 0;
    std::unique_ptr<antlr4::Token> m_pLastToken = nullptr;
}

@lexer::declarations {
    virtual void emit(std::unique_ptr<antlr4::Token> newToken) override;
    std::unique_ptr<antlr4::Token> nextToken(void) override;

    std::unique_ptr<antlr4::Token> createDedent(void);
    std::unique_ptr<antlr4::CommonToken> commonToken(size_t type, const std::string& text);
    std::unique_ptr<antlr4::CommonToken> cloneToken(const std::unique_ptr<antlr4::Token>& source);
    static int getIndentationCount(const std::string& spaces);
    bool atStartOfInput(void);
}

@lexer::definitions {
    void StplDslLexer::emit(std::unique_ptr<antlr4::Token> newToken) {
        m_tokens.push_back(cloneToken(newToken));
        setToken(std::move(newToken));
    }

    std::unique_ptr<antlr4::Token> StplDslLexer::nextToken() {
        if(_input->LA(1) == EOF && !m_indents.empty()) {
            for(int i = m_tokens.size() - 1; i >= 0; --i) {
                if(m_tokens[i]->getType() == EOF) {
                    m_tokens.erase(m_tokens.begin() + i);
                }
            }

            emit(commonToken(StplDslParser::NEWLINE, "\n"));

            while(!m_indents.empty()) {
                emit(createDedent());
                m_indents.pop();
            }

            emit(commonToken(EOF, "<EOF>"));
        }

        std::unique_ptr<antlr4::Token> next = Lexer::nextToken();

        if(next->getChannel() == antlr4::Token::DEFAULT_CHANNEL) {
            m_pLastToken = cloneToken(next);
        }

        if(!m_tokens.empty()) {
            next = std::move(*m_tokens.begin());
            m_tokens.erase(m_tokens.begin());
        }

        return next;
    }

    std::unique_ptr<antlr4::Token> StplDslLexer::createDedent(void) {
        std::unique_ptr<antlr4::CommonToken> dedent;
        dedent = commonToken(StplDslParser::DEDENT, "");

        return dedent;
    }

    std::unique_ptr<antlr4::CommonToken> StplDslLexer::commonToken(size_t type, const std::string& text) {
        int stop, start;

        stop = getCharIndex() - 1;
        start = text.empty() ? stop : stop - text.size() + 1;

        if(m_pLastToken != nullptr) {
            // a last Token exists
            return _factory->create({this, _input}, type, text,
                                    DEFAULT_TOKEN_CHANNEL,
                                    start, stop,
                                    m_pLastToken->getLine(),
                                    m_pLastToken->getCharPositionInLine());
        }
        else {
            // a last Token does not exist
            return (std::unique_ptr<antlr4::CommonToken>) new antlr4::CommonToken({this, _input}, type,
                                                                                  DEFAULT_TOKEN_CHANNEL,
                                                                                  start, stop);
        }
    }

    std::unique_ptr<antlr4::CommonToken> StplDslLexer::cloneToken(const std::unique_ptr<antlr4::Token>& source) {
        return _factory->create({this, _input}, source->getType(), source->getText(),
                               source->getChannel(),
                               source->getStartIndex(), source->getStopIndex(),
                               source->getLine(),
                               source->getCharPositionInLine());
    }

    int StplDslLexer::getIndentationCount(const std::string& spaces) {
        int count = 0;

        for(char ch : spaces) {
            switch(ch) {
                case '\t':
                    count += 8 - (count % 8);
                    break;
                default:
                    ++count;
            }
        }

        return count;
    }

    bool StplDslLexer::atStartOfInput(void) {
        return getCharPositionInLine() == 0 && getLine() == 1;
    }
}
