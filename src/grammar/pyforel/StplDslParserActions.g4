parser grammar StplDslParserActions;

@parser::declarations {
    virtual void setTranslation(std::string) = 0;
    virtual std::string getTranslation(void) = 0;
    virtual void appendStatement(std::string) = 0;

    virtual std::string conjunctSimpleStmts(std::string, std::string) = 0;
    virtual std::string getSimpleStmts(std::string, std::string) = 0;

    virtual std::string getTempTranslation(void) = 0;

    virtual bool addFuncDefinition(antlr4::CommonToken, std::vector<std::string>, std::string) = 0;
    virtual std::string getFuncTranslation(antlr4::CommonToken, std::vector<std::string>) = 0;

    virtual std::string createElifStmt(std::string, std::string, std::string, std::string) = 0;
    virtual std::string createIfStmt(std::string, std::string, std::string, std::string) = 0;

    virtual std::string translateNameList(std::string, antlr4::CommonToken) = 0;
    virtual std::string translateFreezeTime(std::string, std::string, std::string, std::string body) = 0;

    virtual std::string translateBounds(std::string, std::string) = 0;
    virtual std::string translateFutureTemporal(std::string, std::string, std::string) = 0;

    virtual std::string parenthesize(std::string) = 0;
    virtual std::string conjunct(std::string, std::string) = 0;

    virtual std::string reduceString(std::string) = 0;

    virtual void printTranslation(void) = 0;
}
