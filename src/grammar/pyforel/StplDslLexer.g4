lexer grammar StplDslLexer;
import StplDslLexerActions;

NEWLINE
    : ({atStartOfInput()}? SPACES | ('\r'? '\n' | '\r' | '\f') SPACES?)
    {
        {
            std::string newLine, spaces;
            std::string text = getText();

            for(char ch : text) {
                if((ch == '\r') || (ch == '\n') || (ch == '\f')) {
                    newLine.push_back(ch);
                }
                else {
                    spaces.push_back(ch);
                }
            }

            int next      = _input->LA(1);
            int next_next = _input->LA(2);

            if(m_opened > 0 || (next_next != -1 && (next == '\r' || next == '\n' || next == '\f' || next == '#'))) {
                skip();
            }
            else {
                emit(commonToken(NEWLINE, newLine));

                int indent = getIndentationCount(spaces);
                int previous = m_indents.empty() ? 0 : m_indents.top();

                if(indent == previous) {
                    skip();
                }
                else if(indent > previous) {
                    m_indents.push(indent);
                    emit(commonToken(StplDslParser::INDENT, spaces));
                }
                else {
                    while(!m_indents.empty() && (m_indents.top() > indent)) {
                        emit(createDedent());
                        m_indents.pop();
                    }
                }
            }
        }
    }
;

LPAREN
    : '(' ;

RPAREN
    : ')' ;

IMPORT
    : 'import' ;

COMMA
    : ',' ;

AS
    : 'as' ;

VERB
    : 'verb' ;

IF
    : 'if';

ELIF
    : 'elif' ;

ELSE
    : 'else' ;

EXISTS
    : 'exists' ;

FORALL
    : 'forall' ;

FUNC
    : 'func' ;

TEMPORAL_OP
    : ('next' | 'eventually' | 'always') ;

TO
    : 'to' ;

FROM
    : 'from' ;

AT
    : 'at' ;

AND
    : 'and' ;

OR
    : 'or' ;

NOT
    : 'not' ;

MINUS
    : '-' ;

DOT
    : '.' ;

TYPE
    : ('object' | 'time' | 'const') ;

BOOLEAN
    : ('true' | 'false') ;

COLON
    : ':' ;

SEMICOLON
    : ';' ;

CONTINUE
    : 'continue' ;

NAME
    : ID_START ID_CONTINUE* ;

NUMBER
    : (MINUS)? INT_NUMBER
    | (MINUS)? FLOAT_NUMBER
    ;

INT_NUMBER
    : DECIMAL_INTEGER ;

FLOAT_NUMBER
    : POINT_FLOAT ;

fragment DECIMAL_INTEGER
    : NON_ZERO_DIGIT DIGIT*
    | '0'+
    ;

fragment NON_ZERO_DIGIT
    : [1-9] ;

fragment DIGIT
    : [0-9] ;

fragment POINT_FLOAT
    : DIGIT? FRACTION
    | DIGIT '.'
    ;

fragment FRACTION
    : '.' DIGIT+
    ;

fragment Digits
    : [0-9]+ ;

SKIP_
    : (SPACES | COMMENT) -> skip ;

STRING
    : '"' .*? '"' ;

fragment SPACES
    : [ \t]+ ;

fragment COMMENT
    : '#' ~[\r\n\f]*
    | '%' ~[\r\n\f]*
    | '//' ~[\r\n\f]*
    | '/*' .*? '*/'
;

fragment ID_START
    : '_'
    | [A-Z]
    | [a-z]
;

fragment ID_CONTINUE
    : ID_START
    | [0-9]
;
