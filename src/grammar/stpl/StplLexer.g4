// Spatio-Temporal Perception Logic (STPL) Lexer Grammar

lexer grammar StplLexer;

options {
    language = Cpp ;
}

WS
    : [ \t\r\n]+ -> skip ;

SET
    : ('EMPTYSET' | 'UNIVERSE') ;

LPAREN
    : '(' ;

RPAREN
    : ')' ;

LBRACK
    : '[' ;

RBRACK
    : ']' ;

AT
    : '@' ;

COMMA
    : ',' ;

UNDERSCORE
    : '_' ;

NEGATION
    : ('~' | '!' | 'not') ;

MINUS
    : '-' ;

RELOP
    : ('<' | '>' | '<=' | '>=') ;

EQUALITYOP
    : ('==' | '!=') ;

NEXTOP
    : ('next' | 'X') ('_')? (TIME_RNGE | FRAME_RNGE)? ;

FUTUREOP
    : ('finally' | 'eventually' | 'F' | '<>') ('_')? (TIME_RNGE | FRAME_RNGE)? ;

GLOBALLYOP
    : ('globally' | 'always' | 'G' | '[]')+ ('_')? (TIME_RNGE | FRAME_RNGE)? ;

UNTILOP
    : ('until' | 'U') ('_')? (TIME_RNGE | FRAME_RNGE)? ;

RELEASEOP
    : ('release' | 'R') ('_')? (TIME_RNGE | FRAME_RNGE)? ;

UNTILNSOP
    : ('untilns' | 'Uns') ('_')? (TIME_RNGE | FRAME_RNGE)? ;

RELEASENSOP
    : ('releasens' | 'Rns') ('_')? (TIME_RNGE | FRAME_RNGE)? ;

LOP
    : (AndOp | OrOp) ;

fragment AndOp
    : ('and' | '/\\' | '&&' | '&') ;

fragment OrOp
    : ('or' | '\\/' | '||' | '|') ;

QUANTIFIEROP
    : (ExistsOp | ForAllOp) ;

fragment ExistsOp
    : ('EXISTS' | 'E') ;

fragment ForAllOp
    : ('FORALL' | 'A') ;

SPQUANTIFIEROP
    : ('is_nonempty' | 'is_universe') ;

PROPOP
    : (IMPLIESOP | EQUIVOP) ;

IMPLIESOP
    : ('implies' | '->') ;

EQUIVOP
    : ('iff' | '<->') ;

SPACEOP
    : ('I' | 'C') ;

// Constants
CONST_TIME
    : 'C_TIME' ;

CONST_FRAME
    : 'C_FRAME' ;

CRT
    : ('LM' | 'RM' | 'TM' | 'BM' | 'CT') ;

// Literals
BOOLEAN
    : (TRUE | FALSE) ;

fragment TRUE
    : ('TRUE' | 'true') ;

fragment FALSE
    : ('FALSE' | 'false') ;

REAL
    : (DIGIT '.' DIGIT) ;

INT
    : (DIGIT) ;

fragment DIGIT
    : [0-9]+ ;

FUNC_CC
    : 'CC' ;

FUNC_AREA
    : 'area' ;

FUNC_RATIO
    : 'ratio' ;

FUNC_DIST
    : 'dist' ;

FUNC_LAT
    : 'lat_dist' ;

FUNC_LON
    : 'lon_dist' ;

FUNC_CLASS
    : 'class' ;

FUNC_PROB
    : 'prob' ;

NAME
    : LETTER (LETTER | [0-9] | '_')* ;

fragment PREFIX
    : 'Var_' ;

fragment LETTER
    : [a-zA-Z]+ ;

fragment TIME_RNGE
    : 'ts' ;

fragment FRAME_RNGE
    : 'fr:' ;
