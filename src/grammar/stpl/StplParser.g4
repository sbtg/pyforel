parser grammar StplParser;
import StplActions;

options {
    language = Cpp ;
    tokenVocab = StplLexer ;
}

stplSpecification
    : phi EOF;

phi
    : LPAREN phi RPAREN                         #parenPhiExpr

    | BOOLEAN                                   #boolAtom
    | NEGATION phi                              #opNegExpr

    | NEXTOP          (interval)? phi           #opNextExpr
    | FUTUREOP        (interval)? phi           #opFutureExpr
    | GLOBALLYOP      (interval)? phi           #opGloballyExpr

    | phi UNTILOP     (interval)? phi           #opUntilExpr
    | phi RELEASEOP   (interval)? phi           #opReleaseExpr
    | phi UNTILNSOP   (interval)? phi           #opUntilNotStrictExpr
    | phi RELEASENSOP (interval)? phi           #opReleaseNotStrictExpr

    | phi LOP phi                               #opLogicalExpr
    | phi PROPOP phi                            #opPropExpr

    | CONST_TIME  MINUS NAME RELOP (REAL | INT)
        {lookupSymbol($NAME, NUMBER);}          #timeConstraintExpr

    | CONST_FRAME MINUS NAME RELOP INT
        {lookupSymbol($NAME, NUMBER);}          #frameConstraintExpr

    | var1=NAME EQUALITYOP var2=NAME
        {lookupSymbol($var1, OBJECT);
         lookupSymbol($var2, OBJECT);}           #variableEquivalenceExpr

    | freezeTime phi
        {popScope();}                           #opFreezeTimeExpr

    | SPQUANTIFIEROP tau                        #tauExpr
    | pi                                        #piExpr
    | theta                                     #thetaExpr

    | NAME                                      #predicateExpr
;

tau
    : LPAREN tau RPAREN                         #parenTauExpr

    | SET                                       #setTauExpr
    | funcBB                                    #funcBoundingBoxTauExpr

    | NEGATION tau                              #opNegationTauExpr
    | tau LOP tau                               #opLogicalTauExpr
    | SPACEOP tau                               #opSpaceTauExpr

    | NEXTOP          (interval)? tau           #opNextTauExpr
    | FUTUREOP        (interval)? tau           #opFutureTauExpr
    | GLOBALLYOP      (interval)? tau           #opGloballyTauExpr

    | phi UNTILOP     (interval)? tau           #opUntilTauExpr
    | phi RELEASEOP   (interval)? tau           #opReleaseTauExpr
    | phi UNTILNSOP   (interval)? tau           #opUntilNotStrictTauExpr
    | phi RELEASENSOP (interval)? tau           #opReleaseNotStrictTauExpr
;

pi
    : LPAREN pi RPAREN                          #parenPiExpr
    | funcAreaTau      RELOP REAL               #funcAreaTauExpr
    | funcRatioAreaTau RELOP REAL               #funcRatioAreaTauExpr
;

theta
    : LPAREN theta RPAREN                       #parenThetaExpr

    | funcDist    RELOP REAL                    #funcEuclideanDistancExpr
    | funcLatDist RELOP REAL                    #funcLatituteExpr
    | funcLonDist RELOP REAL                    #funcLongitudeExpr

    | funcRatioLatLon RELOP REAL                #funcRatioLatLonExpr

    | funcAreaTheta RELOP REAL                  #funcAreaThetaExpr
    | funcRatioArea RELOP REAL                  #funcRatioAreaThetaExpr

    | funcClass RELOP INT                       #funcClassExpr
    | funcClass EQUALITYOP funcClass            #funcCompareClassExpr
    | funcClass EQUALITYOP NAME                 #funcTypeCategoryExpr

    | funcProb      RELOP REAL                  #funcProbExpr
    | funcRatioProb RELOP REAL                  #funcRatioProbExpr
;

freezeTime
    @init {pushScope();}

    : AT LPAREN (timeVarDecl | objVarDecl | timeVarDecl COMMA objVarDecl) RPAREN
;

timeVarDecl
    : (var=NAME | UNDERSCORE)
        {if($var != NULL) insertSymbol($var, NUMBER);}
;

objVarDecl
    : QUANTIFIEROP COMMA varList;

varList
    : (var=NAME | var=NAME COMMA varList)
        {insertSymbol($var, OBJECT);}
;

interval
    : (LPAREN | LBRACK) (INT | REAL) COMMA (INT | REAL) (RPAREN | RBRACK)
    | (LPAREN | LBRACK) INT COMMA INT (RPAREN | RBRACK)
;

funcBB
    : FUNC_CC LPAREN var=NAME RPAREN
        {lookupSymbol($var, OBJECT);}
;

funcAreaTau
    : FUNC_AREA LPAREN tau RPAREN ;

funcRatioAreaTau
    : FUNC_RATIO LPAREN funcAreaTau COMMA funcAreaTau RPAREN ;

funcDist
    : FUNC_DIST LPAREN var1=NAME COMMA CRT COMMA var2=NAME COMMA CRT RPAREN
        {lookupSymbol($var1, OBJECT);
         lookupSymbol($var2, OBJECT);}
;

funcLatDist
    : FUNC_LAT LPAREN NAME COMMA CRT RPAREN
        {lookupSymbol($NAME, OBJECT);}
;

funcLonDist
    : FUNC_LON LPAREN NAME COMMA CRT RPAREN
        {lookupSymbol($NAME, OBJECT);}
;

funcRatioLatLon
    : FUNC_RATIO LPAREN (funcLatDist | funcLonDist) COMMA (funcLatDist | funcLonDist) RPAREN ;

funcAreaTheta
    : FUNC_AREA LPAREN NAME RPAREN
        {lookupSymbol($NAME, OBJECT);}
;

funcRatioArea
    : FUNC_RATIO LPAREN funcAreaTau COMMA funcAreaTau RPAREN ;

funcClass
    : FUNC_CLASS LPAREN NAME RPAREN
        {lookupSymbol($NAME, OBJECT);}
;

funcProb
    : FUNC_PROB LPAREN NAME RPAREN
        {lookupSymbol($NAME, OBJECT);}
;

funcRatioProb
    : FUNC_RATIO LPAREN funcProb COMMA funcProb RPAREN ;
