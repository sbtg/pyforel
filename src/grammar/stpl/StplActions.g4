parser grammar StplActions;

tokens {OBJECT, NUMBER}

@parser::declarations {
virtual void pushScope(void) = 0;
virtual void popScope(void) = 0;

virtual void insertSymbol(antlr4::Token *token, int type) = 0;
virtual void lookupSymbol(antlr4::Token *token, size_t expected_type) = 0;

// print methods
virtual void printTable(void) = 0;
}
