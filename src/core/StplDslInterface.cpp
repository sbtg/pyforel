#include <iostream>
#include <fstream>

#include "StplDslInterface.h"
#include "Function.h"
#include "StplDslErrorListenerInterface.h"

/*** stplTranslatorInterface Method Definitions ***/
void
StplDslInterface::setErrorInterface(StplDslErrorListenerInterface *e) {
    this->stplError = e;
}

void
StplDslInterface::setTranslation(std::string str) {
    this->translation = str;
}

std::string
StplDslInterface::getTranslation(void) {
    return this->translation;
}

void
StplDslInterface::appendStatement(std::string str) {
    if(!str.empty()) {
        if(translation.empty()) {
            translation = parenthesize(str);
        } else {
            translation = conjunct(translation, parenthesize(str));
        }
    }
}

std::string
StplDslInterface::parenthesize(std::string str) {
    return "(" + str + ")";
}

std::string
StplDslInterface::conjunct(std::string str1, std::string str2) {
    return str1 + " && " + str2;
}

std::string
StplDslInterface::conjunctSimpleStmts(std::string res, std::string str) {
    if(res.empty()) {
        res = parenthesize(str);
    } else {
        res = conjunct(res, parenthesize(str));
    }

    return res;
}

std::string
StplDslInterface::getSimpleStmts(std::string str, std::string res) {
    if(!res.empty()) {
        return parenthesize(conjunct(str, res));
    } else {
        return str;
    }
}

std::string
StplDslInterface::getTempTranslation(void) {
    return temp;
}

bool
StplDslInterface::addFuncDefinition(antlr4::CommonToken t,
                                           std::vector<std::string> params,
                                           std::string def) {
    std::string name;
    name = t.getText();

    if(functions.find(name) == functions.end()) {
        // function with name does not exist
        if(!params.empty()) {
            Function f(def, params);
            functions.insert(std::pair<std::string, Function>(name, f));
        } else {
            Function f(def);
            functions.insert(std::pair<std::string, Function>(name, f));
        }

        return true;

    } else {
        stplError->throwFuncRedeclError(t);
    }
}

std::string
StplDslInterface::getFuncTranslation(antlr4::CommonToken token, std::vector<std::string> args) {
    // check function definition
    stplError->checkFunctionRef(token, functions);

    std::map<std::string, Function>::iterator iter;
    iter = functions.find(token.getText());

    if(iter != functions.end()) {
        return (iter->second).translateFunction(args);
    } else {
        return "";
    }
}

std::string
StplDslInterface::reduceString(std::string str) {
    // remove end-quotes
    str = str.substr(1, str.size() - 2);

    // remove additional space characters
    int i = 0;
    int j = i + 1;

    while(j < str.size()) {
        if(str[i] == ' ' && str[j] == ' ') {
            while(str[j] == ' ') {
                ++j;
            }

            if(str[i] == ' ' && str[j - 1] == ' ') {
                str.erase(str.begin() + i, str.begin() + (j - 1));
                j = i + 1;
            }
        }

        ++i, ++j;
    }

    // replace all newlines with spaces
    std::replace(str.begin(), str.end(), '\n', ' ');

    // remove tabs, carriage returns, and form feeds
    str.erase(remove(str.begin(), str.end(), '\t'), str.end());
    str.erase(remove(str.begin(), str.end(), '\r'), str.end());
    str.erase(remove(str.begin(), str.end(), '\f'), str.end());

    return str;
}

std::string
StplDslInterface::createElifStmt(std::string res,
                                        std::string ifcond,
                                        std::string elcond,
                                        std::string elbody) {
    std::string tmp1;

    if(res.empty()) {
        ifcond = parenthesize(ifcond);
        temp = parenthesize(("!" + conjunct(ifcond, elcond)));

        res = parenthesize((temp + " -> " + parenthesize(elbody)));
    } else {
        temp = parenthesize(temp);
        temp = parenthesize(("!" + conjunct(temp, elcond)));

        tmp1 = parenthesize(temp);
        tmp1 = parenthesize((tmp1 + " -> " + parenthesize(elbody)));

        res = conjunct(res, tmp1);
    }

    return res;
}

std::string
StplDslInterface::createIfStmt(std::string ret,
                                      std::string ifcond,
                                      std::string ifbody,
                                      std::string elsbody) {
    std::string result, tmp1, tmp2;

    tmp1 = parenthesize(ifcond);
    result = parenthesize((tmp1 + " -> " + ifbody));

    if(!ret.empty()) {
        result = conjunct(result, ret);
    }

    if(!elsbody.empty()) {
        if(!ret.empty()) {
            temp = parenthesize(temp);
            temp = parenthesize(("!" + temp + " -> " + elsbody));

            result = conjunct(result, temp);
        } else {
            tmp2 = parenthesize(ifcond);
            tmp2 = parenthesize("!" + tmp2);
            tmp2 = parenthesize((tmp2 + " -> " + parenthesize(elsbody)));

            result = conjunct(result, tmp2);
        }
    }

    temp.clear();

    return result;

}

std::string
StplDslInterface::translateNameList(std::string res, antlr4::CommonToken name) {
    if(res.empty()) {
        res = name.getText();
    } else {
        res = res + ", " + name.getText();
    }

    return res;
}

std::string
StplDslInterface::translateFreezeTime(std::string res,
                                             std::string quantifier,
                                             std::string time,
                                             std::string body) {
    if(quantifier.compare("exists") == 0)
        quantifier = "EXISTS";
    else if(quantifier.compare("forall") == 0)
        quantifier = "FORALL";

    if(!time.empty()) {
        res = parenthesize("@(" + time + ", " + quantifier + ", " + res + ") " + body);
    } else {
        res = parenthesize("@(" + quantifier + ", " + res + ") " + body);
    }

    return res;
}

std::string
StplDslInterface::translateBounds(std::string lower,
                                         std::string upper) {
    std::string result;
    result = "[" + lower + ", " + upper + "]";

    return result;
}

std::string
StplDslInterface::translateFutureTemporal(std::string res,
                                                 std::string op,
                                                 std::string body) {

    if(!res.empty()) {
        res = op + res;
    } else {
        res = op;
    }

    res = res + parenthesize(body);
    return res;
}

void
StplDslInterface::writeTranslation(char *name) {
    if(stplError->getErrorCount() == 0) {
        std::ofstream stream;
        stream.open(name, std::ios::out);

        stream << translation << std::endl;
        stream.close();
    } else {
        std::cout << "Errors: " << stplError->getErrorCount() << std::endl;
    }
}

void
StplDslInterface::printTranslation(void) {
    if(stplError->getErrorCount() == 0)
        std::cout << translation << std::endl;
    else
        std::cout << "Errors: " << stplError->getErrorCount() << std::endl;
}
