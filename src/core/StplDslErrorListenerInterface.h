#ifndef STPL_DSL_ERROR_LISTENER_INTERFACE_H
#define STPL_DSL_ERROR_LISTENER_INTERFACE_H

#include "antlr4-runtime.h"
#include "Function.h"

#define KEYWORD_COUNT 13

class StplDslErrorListenerInterface : public antlr4::BaseErrorListener {
private:
    std::string keywords[KEYWORD_COUNT] = {"if", "elif", "else", "and",
        "or", "exists", "forall", "at",
        "stpl", "object", "time", "const",
        "import"};

    unsigned int errors = 0;

public:
    void syntaxError(antlr4::Recognizer *recognizer,
                     antlr4::Token * offendingSymbol,
                     size_t line,
                     size_t charPositionInLine,
                     const std::string &msg,
                     std::exception_ptr e) override;

    bool checkKeyword(antlr4::CommonToken token);
    void throwIndentationError(antlr4::CommonToken *token);
    void throwUnexpectedIndentationError(antlr4::CommonToken *token);
    void checkFunctionRef(antlr4::CommonToken token,
                          std::map<std::string, Function> functions);

    void throwFuncRedeclError(antlr4::CommonToken token);

    unsigned int getErrorCount();

    std::string redText(std::string);
    std::string magentaText(std::string);
    std::string boldText(std::string);
};

#endif
