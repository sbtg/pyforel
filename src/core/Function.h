#ifndef FUNCTION_H
#define FUNCTION_H

#include <string>
#include <vector>

class Function {
 private:
    std::vector<std::string> parameters;
    std::string definition;

 public:
    Function(std::string def);
    Function(std::string def, std::vector<std::string> params);

    std::string getDefinition(void);
    std::vector<std::string> getParameters(void);
    std::string translateFunction(std::vector<std::string> args);
    void addParameters(std::string);
};

#endif
