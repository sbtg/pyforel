#include <regex>
#include "Function.h"

Function::Function(std::string def) {
    definition = def;
}

Function::Function(std::string def, std::vector<std::string> params) {
    definition = def;
    parameters = params;
}

std::string Function::getDefinition(void) {
    return definition;
}

std::vector<std::string> Function::getParameters(void) {
    return parameters;
}

    std::string Function::translateFunction(std::vector<std::string> args) {
        std::string translated = definition;

        for(int i = 0; i < parameters.size(); ++i) {
            std::string expr;
            expr = "\\b" + parameters.at(i) + "\\b";

            std::regex e(expr);
            translated = std::regex_replace(translated, e, args.at(i));
        }

        return translated;
    }

void Function::addParameters(std::string param) {
    parameters.push_back(param);
}
