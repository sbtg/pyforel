#ifndef STPL_TRANSLATOR_INTERFACE_H
#define STPL_TRANSLATOR_INTERFACE_H

#include <string>
#include "StplDslParser.h"
#include "Function.h"

/* forward declarations */
class StplDslErrorListenerInterface;

class StplDslInterface : public StplDslParser {
private:
    std::string translation;
    std::map<std::string, Function> functions;

    std::string temp;

    StplDslErrorListenerInterface *stplError;

public:
    StplDslInterface(antlr4::TokenStream *stream)
    : StplDslParser(stream) {
        // call superclass constructor with TokenStream
    }

    void setErrorInterface(StplDslErrorListenerInterface *errorListener);

    void setTranslation(std::string) override;
    std::string getTranslation(void) override;
    void appendStatement(std::string) override;

    std::string conjunctSimpleStmts(std::string, std::string) override;
    std::string getSimpleStmts(std::string, std::string) override;

    std::string getTempTranslation(void) override;

    bool addFuncDefinition(antlr4::CommonToken, std::vector<std::string>, std::string) override;
    std::string getFuncTranslation(antlr4::CommonToken, std::vector<std::string>) override;

    std::string createElifStmt(std::string, std::string, std::string, std::string) override;
    std::string createIfStmt(std::string, std::string, std::string, std::string) override;

    std::string translateNameList(std::string, antlr4::CommonToken) override;
    std::string translateFreezeTime(std::string, std::string, std::string, std::string body) override;

    std::string translateBounds(std::string, std::string) override;
    std::string translateFutureTemporal(std::string, std::string, std::string) override;

    std::string parenthesize(std::string) override;
    std::string conjunct(std::string, std::string) override;

    std::string reduceString(std::string) override;

    void writeTranslation(char *out_filename);
    void printTranslation(void) override;
};

#endif
