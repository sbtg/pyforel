#ifndef STPL_ERROR_LISTENER_INTERFACE_H
#define STPL_ERROR_LISTENER_INTERFACE_H

#include <string>
#include "antlr4-runtime.h"

class StplErrorListenerInterface : public antlr4::BaseErrorListener {
 private:
    unsigned int errors = 0;

 public:
    int getErrorCount(void);

    void syntaxError(antlr4::Recognizer *recognizer,
                     antlr4::Token * offendingSymbol,
                     size_t line,
                     size_t charPositionInLine,
                     const std::string &msg,
                     std::exception_ptr e) override;

    /* semantic errors */
    void throwVarRedeclError(antlr4::Token *token);
    void throwVarRefNotInScopeError(antlr4::Token *token);
    void throwVarRefTypeError(antlr4::Token *token, antlr4::CommonToken *t, size_t type);
    void throwUndeclRefError(antlr4::Token *token);

    std::string redText(std::string);
    std::string magentaText(std::string);
    std::string boldText(std::string);
};

#endif
