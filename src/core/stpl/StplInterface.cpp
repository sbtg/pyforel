#include <iostream>
#include "StplInterface.h"
#include "StplErrorListenerInterface.h"

void
StplInterface::setErrorInterface(StplErrorListenerInterface *errorListener) {
    this->e = errorListener;
}

void
StplInterface::pushScope(void) {
    scope *sc;
    sc = new scope;
    sc->frozen = false;

    tables.push_back(sc);

    // increment current scope index
    ++(this->depth);
}

void
StplInterface::popScope(void) {
    scope *sc;
    sc = tables.back();

    // free symbols in scope table
    map<string, antlr4::CommonToken *>::iterator iter;
    for(iter = (sc->table).begin(); iter != (sc->table).end(); ++iter) {
        antlr4::CommonToken *t = (*iter).second;
        delete t;
    }

    // free map symbol table
    delete sc;

    // remove scope from table stack
    tables.pop_back();

    // decrement current scope index
    --depth;
}

void
StplInterface::insertSymbol(antlr4::Token *token, int type) {
    // create CommonToken
    antlr4::CommonToken *t = new antlr4::CommonToken(token);
    t->setType(type);

    scope *sc;
    sc = this->tables[this->depth];

    if(type == StplParser::NUMBER)
        sc->frozen = true;

    if((sc->table).find(t->getText()) != (sc->table).end())
        (this->e)->throwVarRedeclError(token);
    else
        (sc->table).insert(pair<string, antlr4::CommonToken *>(t->getText(), t));
}

void
StplInterface::lookupSymbol(antlr4::Token *token, size_t expected_type) {
    antlr4::CommonToken *t;
    int frzn_count = 0;

    int i = this->tables.size() - 1;
    vector<scope *>::reverse_iterator riter;

    // iteratively search for the variable
    for(riter = this->tables.rbegin(); riter != this->tables.rend(); ++riter, --i) {
        scope *sc;
        map<string, antlr4::CommonToken *>::iterator it;
        sc = (*riter);

        // check if scope is frozen
        if(sc->frozen)
            ++frzn_count;

        if(frzn_count > 1) {
            (this->e)->throwVarRefNotInScopeError(token);
            return;
        }

        // search if variable on scope
        it = (sc->table).find(token->getText());
        if(it != (sc->table).end()) {
            // check the variable type is correct
            if(((*it).second)->getType() == expected_type)
                return;
            else {
                (this->e)->throwVarRefTypeError(token, (*it).second, expected_type);
                return;
            }
        }
    }

    (this->e)->throwUndeclRefError(token);
    return;
}

void
StplInterface::printTable(void) {
    cout << "~#################" << endl;

    for(int i = 0; i < this->tables.size(); ++i) {
        scope *sc;
        sc = this->tables[i];

        cout << setfill(' ') << setw(9) << (sc->frozen ? "(FROZEN) " : "");
        cout << "SCOPE " << setfill(' ') << setw(3) << i << ": ";
        cout << "{";

        map<string, antlr4::CommonToken *>::iterator iter;
        for(iter = (sc->table).begin(); iter != (sc->table).end() &&
                next(iter) != (sc->table).end(); ++iter) {
            cout << ((*iter).second)->getText() << ":";

            if(((*iter).second)->getType() == StplParser::OBJECT)
                cout << "OBJECT";
            else if(((*iter).second)->getType() == StplParser::NUMBER)
                cout << "NUMBER";

            cout << ", ";
        }

        cout << ((*iter).second)->getText() << ":";

        if(((*iter).second)->getType() == StplParser::OBJECT)
            cout << "OBJECT";
        else if(((*iter).second)->getType() == StplParser::NUMBER)
            cout << "NUMBER";

        cout << "}\n";
    }
}
