#ifndef STPL_INTERFACE_H
#define STPL_INTERFACE_H

#include <vector>
#include <map>
#include "StplParser.h"

/* forward declarations */
class StplErrorListenerInterface;

/* namespace usage */
using namespace std;

typedef struct Scopes {
    bool frozen;
    map<string, antlr4::CommonToken *> table;
} scope;

class StplInterface : public StplParser {
private:
    vector<scope *> tables; // list of symbol tables
    unsigned int depth;                        // index of active table

    StplErrorListenerInterface *e;

public:
    StplInterface(antlr4::TokenStream *tokens)
        : StplParser(tokens) {
        // call parent class constructor
        this->depth = -1;
    }

    void setErrorInterface(StplErrorListenerInterface *errorListener);

    void pushScope(void) override;
    void popScope(void) override;

    void insertSymbol(antlr4::Token *token, int type) override;
    void lookupSymbol(antlr4::Token *token, size_t expected_type) override;

    // print methods
    void printTable(void) override;
};

#endif
