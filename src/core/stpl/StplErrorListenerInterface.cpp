#include "StplErrorListenerInterface.h"
#include "StplParser.h"

int
StplErrorListenerInterface::getErrorCount(void) {
    return this->errors;
}

void
StplErrorListenerInterface::syntaxError(antlr4::Recognizer *recognizer,
                                        antlr4::Token *offendingSymbol,
                                        size_t line,
                                        size_t charPositionInLine,
                                        const std::string &msg,
                                        std::exception_ptr e) {

    ++(this->errors);
    std::cerr << line << ":" << charPositionInLine
              << ": "
              << boldText(redText("syntax error"))
              << boldText(": ")
              << msg
              << std::endl;
}

/*** semantic error checking ***/
void
StplErrorListenerInterface::throwVarRedeclError(antlr4::Token *token) {
    ++(this->errors);
    std::cerr << token->getLine() << ":" << token->getCharPositionInLine()
              << ": "
              << boldText(redText("error: "))
              << boldText("\u2018" + token->getText() + "\u2019")
              << " previously declared."
              << std::endl;
    return;
}

void
StplErrorListenerInterface::throwVarRefNotInScopeError(antlr4::Token *token) {
    std::cerr << token->getLine() << ":" << token->getCharPositionInLine()
              << ": "
              << boldText(redText("error: "))
              << " undefined reference to "
              << boldText("\u2018" + token->getText() + "\u2019")
              << " (ensure the variable is declared within the frozen scope)"
              << std::endl;

}

void
StplErrorListenerInterface::throwVarRefTypeError(antlr4::Token *token,
                                                 antlr4::CommonToken *t,
                                                 size_t type) {
    ++(this->errors);
    std::cerr << token->getLine() << ":" << token->getCharPositionInLine()
              << ": "
              << boldText(redText("error: "))
              << "TypeError: "
              << boldText("\u2018" + token->getText() + "\u2019")
              << ": expected ";

    if(type == StplParser::OBJECT)
        std::cerr << boldText("object");
    else if(type == StplParser::NUMBER)
        std::cerr << boldText("constant");
    else
        std::cerr << boldText("<unknown>");

    std::cerr << ", found ";

    size_t t_type;
    t_type = t->getType();

    if(t_type == StplParser::OBJECT)
        std::cerr << boldText("object");
    else if(t_type == StplParser::NUMBER)
        std::cerr << boldText("constant");
    else
        std::cerr << boldText("<unknown>");

    std::cerr << std::endl;
}

void
StplErrorListenerInterface::throwUndeclRefError(antlr4::Token *token) {
    ++(this->errors);
    std::cerr << token->getLine() << ":" << token->getCharPositionInLine()
              << ": "
              << boldText(redText("error: "))
              << boldText("\u2018" + token->getText() + "\u2019")
              << " undeclared (first use in expression)"
              << std::endl;
}

std::string
StplErrorListenerInterface::redText(std::string str) {
    return "\u001b[31m" + str + "\u001b[0m";
}

std::string
StplErrorListenerInterface::magentaText(std::string str) {
    return "\u001b[35m" + str + "\u001b[0m";
}

std::string
StplErrorListenerInterface::boldText(std::string str) {
    return "\u001b[1m" + str + "\u001b[0m";
}
