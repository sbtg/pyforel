#include "StplDslErrorListenerInterface.h"
#include "StplDslParser.h"

/*** syntactic error checking ***/
void
StplDslErrorListenerInterface::syntaxError(antlr4::Recognizer *recognizer,
                                           antlr4::Token *offendingSymbol,
                                           size_t line,
                                           size_t charPositionInLine,
                                           const std::string &msg,
                                           std::exception_ptr e) {

    ++(this->errors);

    antlr4::misc::IntervalSet interval = ((antlr4::Parser *) recognizer)->getExpectedTokens();
    std::vector<antlr4::misc::Interval> intervals = interval.getIntervals();

    std::vector<ssize_t> tokenList = interval.toList();

    // antlr4::dfa::Vocabulary vocab = recognizer->getVocabulary();
    // std::cout << "EXPECTED TOKEN: " << vocab.getDisplayName(tokenList.at(0)) << std::endl;

    if(checkKeyword(*((antlr4::CommonToken *) offendingSymbol))) {
        return;
    } else if(tokenList.at(0) == StplDslParser::INDENT) {
        throwIndentationError((antlr4::CommonToken *) offendingSymbol);
        return;
    } else if(offendingSymbol->getText()[0] == '\t') {
        throwUnexpectedIndentationError((antlr4::CommonToken *) offendingSymbol);
    } else {
        std::cerr << line << ":" << charPositionInLine
                  << ": "
                  << boldText(redText("syntax error"))
                  << boldText(": ")
                  << msg
                  << std::endl;
    }
}

bool
StplDslErrorListenerInterface::checkKeyword(antlr4::CommonToken token) {
    for(int i = 0; i < KEYWORD_COUNT; ++i) {
        if((token.getText()).compare(keywords[i]) == 0) {
            std::cerr << token.getLine() << ":" << token.getCharPositionInLine()
                      << ": "
                      << boldText(redText("syntax error"))
                      << boldText(": ")
                      << boldText("'" + keywords[i] + "'")
                      << " is a reserved word"
                      << std::endl;

            ++(this->errors);
            return true;
        }
    }

    return false;
}

void
StplDslErrorListenerInterface::throwIndentationError(antlr4::CommonToken *token) {
    std::cerr << token->getLine() << ":" << token->getCharPositionInLine()
              << ": "
              << boldText(redText("syntax error"))
              << boldText(": ")
              << "expected indentation for block"
              << std::endl;
}

void
StplDslErrorListenerInterface::throwUnexpectedIndentationError(antlr4::CommonToken *token) {
    std::cerr << token->getLine() << ":" << token->getCharPositionInLine()
              << ": "
              << boldText(redText("syntax error"))
              << boldText(": ")
              << "unexpected indentation error"
              << std::endl;
}

/*** semantic error checking ***/
void
StplDslErrorListenerInterface::checkFunctionRef(antlr4::CommonToken token,
                                       std::map<std::string, Function> functions) {
    std::map<std::string, Function>::iterator iter;
    iter = functions.find(token.getText());

    if(iter == functions.end()) {
        // function name does not exist
        std::cerr << token.getLine() << ":" << token.getCharPositionInLine()
                  << ": "
                  << boldText(redText("error"))
                  << boldText(": ")
                  << "undefined reference to "
                  << boldText("'" + token.getText() + "'")
                  << std::endl;

        ++(this->errors);
    }
}

void
StplDslErrorListenerInterface::throwFuncRedeclError(antlr4::CommonToken token) {
    std::cerr << token.getLine() << ":" << token.getCharPositionInLine()
              << ": "
              << boldText(redText("error"))
              << boldText(": ")
              << "redeclaration of function "
              << boldText("'" + token.getText() + "'")
              << std::endl;

    ++(this->errors);
}

unsigned int
StplDslErrorListenerInterface::getErrorCount(void) {
    return this->errors;
}

std::string
StplDslErrorListenerInterface::redText(std::string str) {
    return "\u001b[31m" + str + "\u001b[0m";
}

std::string
StplDslErrorListenerInterface::magentaText(std::string str) {
    return "\u001b[35m" + str + "\u001b[0m";
}

std::string
StplDslErrorListenerInterface::boldText(std::string str) {
    return "\u001b[1m" + str + "\u001b[0m";
}
